<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>vinury.vrion</groupId>
	<artifactId>vrion</artifactId>
	<version>0.0.13</version>
	<packaging>jar</packaging>

	<name>vrion</name>
	<description>Demo project for Spring Boot</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.0.1.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<lombok.version>1.16.18</lombok.version>
		<thymeleaf-layout-dialect.version>2.2.2</thymeleaf-layout-dialect.version>
        <thymeleaf-extras-springsecurity4.version>3.0.2.RELEASE</thymeleaf-extras-springsecurity4.version>
		<webjars-locator.version>0.34</webjars-locator.version>
		<bootstrap.version>4.0.0</bootstrap.version>
		<font-awesome.version>4.7.0</font-awesome.version>
        <spring-social.version>1.1.4.RELEASE</spring-social.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

        <!-- Security -->

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>

        <!-- Cloud -->

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-cloud-connectors</artifactId>
        </dependency>

        <!-- Data -->

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-rest</artifactId>
		</dependency>

		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<!-- Check for the most recent available version: https://projectlombok.org/changelog.html -->
			<version>${lombok.version}</version>
		</dependency>

        <!-- Web -->

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
        </dependency>

        <dependency>
            <groupId>nz.net.ultraq.thymeleaf</groupId>
            <artifactId>thymeleaf-layout-dialect</artifactId>
            <version>${thymeleaf-layout-dialect.version}</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.thymeleaf.extras/thymeleaf-extras-springsecurity4 -->
        <dependency>
            <groupId>org.thymeleaf.extras</groupId>
            <artifactId>thymeleaf-extras-springsecurity4</artifactId>
            <version>${thymeleaf-extras-springsecurity4.version}</version>
        </dependency>

        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>webjars-locator</artifactId>
            <version>${webjars-locator.version}</version>
        </dependency>

        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>bootstrap</artifactId>
            <version>${bootstrap.version}</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.webjars/font-awesome -->
        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>font-awesome</artifactId>
            <version>${font-awesome.version}</version>
        </dependency>

		<!-- dependency>
			<groupId>org.webjars.npm</groupId>
			<artifactId>feather-icons</artifactId>
			<version>4.7.0</version>
		</dependency -->

        <!-- Mail -->

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-mail</artifactId>
		</dependency>

        <!-- Social -->

		<!--<dependency>-->
			<!--<groupId>org.springframework.social</groupId>-->
			<!--<artifactId>spring-social-core</artifactId>-->
			<!--<version>${spring-social.version}</version>-->
		<!--</dependency>-->

		<!--<dependency>-->
		<!--<groupId>org.springframework.social</groupId>-->
		<!--<artifactId>spring-social-web</artifactId>-->
		<!--<version>1.1.6.RELEASE</version>-->
		<!--</dependency>-->

		<dependency>
            <groupId>org.springframework.social</groupId>
            <artifactId>spring-social-facebook</artifactId>
            <version>2.0.3.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.social</groupId>
            <artifactId>spring-social-security</artifactId>
            <version>${spring-social.version}</version>
        </dependency>

        <!-- Test -->

        <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>

			<plugin>
					<artifactId>maven-deploy-plugin</artifactId>
					<version>2.8.2</version>
					<configuration>
							<altDeploymentRepository>internal.repo::default::file://${project.basedir}/../../${project.name}-mvn-repo</altDeploymentRepository>
					</configuration>
			</plugin>

		</plugins>
	</build>

</project>
